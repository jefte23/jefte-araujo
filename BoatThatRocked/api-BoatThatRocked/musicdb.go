package main

import (
	"log"

	"github.com/globalsign/mgo/bson"
)

func RegisterMusic(music *Music) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("music")

	err = collection.Insert(music)

	return
}

func MusicCollection() (music []interface{}) {

	conn, _ := GetConnection()

	collection := conn.DB("BoatThatRocked").C("music")

	collection.Find(bson.M{}).All(&music)

	return

}

func RemoveMusic(id string) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("music")

	collection.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	return
}

func FindMusicName(music *NameMusic) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("music")

	err = collection.Find(bson.M{"name": music.Name}).All(&music)

	return
}

func EditMusic(music *Music) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("music")

	err = collection.Update(bson.M{"_id": music.Name}, &music)

	return
}
