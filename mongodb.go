package main

import (
	"fmt"
	"io"
	"log"
	"time"
	//"io/ioutil"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	mgo "gopkg.in/mgo.v2"
)

func main() {

	fmt.Println("Starting server...")

	r := mux.NewRouter()

	r.HandleFunc("/", Home)

	r.HandleFunc("/time", Time).Methods("GET")

	r.HandleFunc("/users", CreateUser).Methods("POST")

	http.ListenAndServe("172.22.51.137:8080", r)
}

func Home(response http.ResponseWriter, request *http.Request) {

	fmt.Printf("Chegou request numero")
	response.Write([]byte("Reprovados na segunda a noite"))
}

func Time(response http.ResponseWriter, request *http.Request) {

	fmt.Printf("Request: %v", request)
	now := time.Now()
	response.Write([]byte("A hora do servidor: " + now.String()))
}

type User struct {
	name      string `json:"name"`
	pass      string `json:"pass"`
	birthdate string `json:"birthdate"`
	gender    string `json:"gender"`
}

func CreateUser(response http.ResponseWriter, request *http.Request) {

	user := User{}

	DecodeJson(request.Body, &user)

	fmt.Printf("Decoder: %v", user)

	response.Write([]byte("{message: user created}"))

}

func DecodeJson(payload io.Reader, entity interface{}) (err error) {

	decoder := json.NewDecoder(payload)

	err = decoder.Decode(entity)

	if err != nil {
		log.Printf("[ERROR] could not convert json, because: %v", err)
		return
	}

	return
}

func cadastro(payload io.Reader, entity interface{}) (err error) {

	session, err := mgo.Dial("localhost:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	c := session.DB("usersDB").C("users")

	test := User{}

	err = c.Insert(test)

	if err != nil {
		log.Printf("[ERROR] Erro 400", err)
	}

	return

}

func busca(payload io.Reader, entity interface{}) (err error) {

	session, err := mgo.Dial("localhost:27017")

	if err != nil {
		panic(err)
	}
	defer session.Close()

	result := User{}

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("User:", result.name)

	r := mux.NewRouter()

}
