package main

import (
	"log"

	"github.com/globalsign/mgo/bson"
)

// Regista artista
func RegisterArtist(artist *Artist) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("artists")

	err = collection.Insert(artist)

	return

}

// Busca todos os artistas no banco de dados
func ArtistCollection() (artists []interface{}) {

	conn, _ := GetConnection()

	collection := conn.DB("BoatThatRocked").C("artists")

	collection.Find(bson.M{}).All(&artists)

	return
}

// Deleta Artista
func RemoveArtist(namename string) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("artists")

	collection.Remove(bson.M{"artistname": bson.ObjectIdHex(namename)})

	return
}

func EditArtist(artist *Artist) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("artists")

	err = collection.Update(bson.M{"nameartist": artist.ArtistName}, &artist)

	return
}

func FindArtistName(artist *nameArtist) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("artists")

	err = collection.Find(bson.M{"name": artist.Name}).All(&artist)

	return
}
