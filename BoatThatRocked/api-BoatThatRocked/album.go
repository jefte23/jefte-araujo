package main

import "net/http"

// struct do json Usuario e a validação da informação passava via URL
type Album struct {
	_id           int64    `json:"_id"`
	Name          string   `json:"name" validate:"required"`
	AlbumInsert   string   `json:"artistname"`
	Artist        []string `json:"photograph"`
	dateOfRelease []string `json:"dateofrelease"`
	Genre         []string `json:"genre" validate:"required"`
	Songs         []bool   `json:"songs"`
}
type AlbumName struct {
	_id  string `json:"_id"`
	Name string `json: "name" validate:"required"`
}

func InsertAlbum(response http.ResponseWriter, request *http.Request) {
	// retirando corpo da requisição
	body := request.Body

	// criando estrutura de usuário vazia
	album := Album{}

	// decodificando o JSON no estrutura de usuário
	err := DecodeJson(body, &album)

	// validando erro de decodificação
	if err != nil {
		// resposta HTTP de formato inválido da informação
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	// validação da informação
	err = validate.Struct(album)

	// validando erro de validação
	if err != nil {
		// resposta HTTP de invalidação dos campos da requisição
		response.Write([]byte(`{"code": 400, "message": "invalid parameters!"}`))
		return
	}

	err = RegisterAlbum(&album)

	// validação na interação com banco de dados
	if err != nil {
		// resposta HTTP de falha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))

}

// Busca todos os album do banco de dados
func SearchAllAlbum(response http.ResponseWriter, request *http.Request) {

	album := AlbumCollection()

	EncodeJson(response, album)
}

// Excluir um Album do bd
func DeleteAlbum(response http.ResponseWriter, request *http.Request) {

	err := RemoveAlbum(request.URL.Query().Get("id"))

	if err != nil {
		response.Write([]byte("code: 400"))
		return
	}

	response.Write([]byte("sucess"))
	return
}

// Busca album pelo nome
func ArtistAlbum(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	album := AlbumName{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &album)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
	}

	err = validate.Struct(album)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
	}

	err = FindAlbumName(&album)

	if err != nil {
		response.Write([]byte("500"))
		return
	}

	response.Write([]byte("200"))
}

func UpdateAlbum(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	album := Album{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &album)

	// Validando erro de decodificação
	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	// validação da informação
	err = validate.Struct(album)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
		return
	}

	// regras de negócio para salvar usuario no banco de dados
	err = EditAlbum(&album)

	// Validação na interação com banco de dados
	if err != nil {
		// resposta HTTP de falaha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))

}
