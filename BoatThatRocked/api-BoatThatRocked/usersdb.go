package main

import (
	"log"

	"github.com/globalsign/mgo/bson"
)

func RegisterUser(user *User) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("users")

	err = collection.Insert(user)

	return

}

func UserCollection() (users []interface{}) {

	conn, _ := GetConnection()

	collection := conn.DB("BoatThatRocked").C("users")

	collection.Find(bson.M{}).All(&users)

	return
}

func RemoveUser(id string) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("users")

	collection.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	return
}

func EditUser(user *UserUpdate) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("users")

	err = collection.Update(bson.M{"name": user._id}, &user)

	return
}

func AccessUser(login *UserLogin) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("users")

	err = collection.Find(bson.M{"email": login.Email, "password": login.Password}).One(&login)

	return
}

func FindAllName(users *UserName) (err error) {
	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("users")

	err = collection.Find(bson.M{"name": users.Name}).All(&users)

	return
}
