package main

import "net/http"

type Music struct {
	_id      int64    `json:"_id"`
	Name     string   `json:"name" validate:"required"`
	Artist   string   `json:"artist" validate:"required"`
	Genre    []string `json:"genre" validate:"required"`
	Composer []string `json:"compose"`
	Duration string   `json:"duration"`
}

type NameMusic struct {
	Name string `json:"name" validate:"required"`
}

func InsertMusic(response http.ResponseWriter, request *http.Request) {

	body := request.Body

	music := Music{}

	err := DecodeJson(body, &music)

	// validando erro de decodificação
	if err != nil {
		// resposta HTTP de formato inválido da informação
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	err = validate.Struct(music)

	if err != nil {
		// resposta HTTP de invalidação dos campos da requisição
		response.Write([]byte(`{"code": 400, "message": "invalid parameters!"}`))
		return
	}

	err = RegisterMusic(&music)

	// validação na interação com banco de dados
	if err != nil {
		// resposta HTTP de falha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))

}

// Busca todos os artistas do banco de dados
func SearchAllMusic(response http.ResponseWriter, request *http.Request) {

	music := MusicCollection()

	EncodeJson(response, music)
}

// Excluir um artista do bd atravez do nome artistico dele
func DeleteMusic(response http.ResponseWriter, request *http.Request) {

	err := RemoveMusic(request.URL.Query().Get("_id"))

	if err != nil {
		response.Write([]byte("code: 400"))
		return
	}

	response.Write([]byte("sucess"))
	return
}

func MusicName(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	music := NameMusic{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &music)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
	}

	err = validate.Struct(music)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
	}

	err = FindMusicName(&music)

	if err != nil {
		response.Write([]byte("500"))
		return
	}

	response.Write([]byte("200"))
}

func UpdateMusic(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	music := Music{}

	err := DecodeJson(body, &music)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	err = validate.Struct(music)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
		return
	}

	err = EditMusic(&music)

	if err != nil {
		// resposta HTTP de falaha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))

}
