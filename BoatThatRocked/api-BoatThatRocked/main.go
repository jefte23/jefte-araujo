package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	startServer()
}

func startServer() {

	StartValidator()

	r := mux.NewRouter()

	// Função que chama registro de usuário
	r.HandleFunc("/users", InsertUser).Methods("POST")

	// Função para excluir um usuário
	r.HandleFunc("/users/delete", DeleteUser).Methods("PUT")

	// alterar dados do usuario
	r.HandleFunc("/users", UpdateUser).Methods("PUT")

	// Login do Usuário
	r.HandleFunc("/users/login", LoginUser).Methods("POST")

	// Função que Busca um usuário
	r.HandleFunc("/users", SearchAllUsers).Methods("GET")

	// Busca usuarios pelo nome
	r.HandleFunc("/users", UsersName).Methods("GET")

	// -------------- Chamadas referentes ao artista ---------------------

	// Cadastrar Artista
	r.HandleFunc("/users", InsertArtist).Methods("POST")

	//Busca todos os artistas
	r.HandleFunc("/users", SearchAllArtist).Methods("GET")

	// Exclui um artista recebendo seu nome artistico
	r.HandleFunc("/users/delete", DeleteArtist).Methods("PUT")

	// Função que Busca artista por nome
	r.HandleFunc("/users", ArtistName).Methods("GET")

	// alterar dados do artista
	r.HandleFunc("/album", UpdateArtist).Methods("PUT")

	// -------------- Chamadas referentes ao Album ---------------------
	// Cadastrar Album
	r.HandleFunc("/album", InsertAlbum).Methods("POST")

	//Busca todos os Album
	r.HandleFunc("/album", SearchAllAlbum).Methods("GET")

	// Exclui um Album recebendo seu nome artistico
	r.HandleFunc("/album/delete", DeleteAlbum).Methods("PUT")

	// Função que Busca Album por nome
	r.HandleFunc("/album", ArtistAlbum).Methods("GET")

	// alterar dados do Album
	r.HandleFunc("/album", UpdateAlbum).Methods("PUT")

	// -------------- Chamadas referentes ao Music ---------------------

	// Cadastrar Musica
	r.HandleFunc("/album", InsertMusic).Methods("POST")

	//Busca todos os Musica
	r.HandleFunc("/album", SearchAllMusic).Methods("GET")

	// Exclui um Musica recebendo seu nome ID
	r.HandleFunc("/album/delete", DeleteMusic).Methods("PUT")

	// Função que Busca Musica por nome
	r.HandleFunc("/album", MusicName).Methods("GET")

	// alterar dados do Musica
	r.HandleFunc("/album", UpdateMusic).Methods("PUT")

	http.ListenAndServe(SERVER_HOST, r)
}
