package main

import "net/http"

// struct do json Usuario e a validação da informação passava via URL
type Artist struct {
	_id         int64  `json:"_id"`
	Name        string `json:"name" validate:"required"`
	ArtistName  string `json:"artistname"`
	Photograph  string `json:"photograph"`
	dateOfBirth string `json:"dateOfBirth" validate:"required"`
	Password    string `json:"password" validate:"required"`
	tour        bool   `json:"tour"`
}

// struct Json para Busca de artista pelo nome
type nameArtist struct {
	Name string `json:"name" validate:"required"`
}

func InsertArtist(response http.ResponseWriter, request *http.Request) {
	// retirando corpo da requisição
	body := request.Body

	// criando estrutura de usuário vazia
	artist := Artist{}

	// decodificando o JSON no estrutura de usuário
	err := DecodeJson(body, &artist)

	// validando erro de decodificação
	if err != nil {
		// resposta HTTP de formato inválido da informação
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	// validação da informação
	err = validate.Struct(artist)

	// validando erro de validação
	if err != nil {
		// resposta HTTP de invalidação dos campos da requisição
		response.Write([]byte(`{"code": 400, "message": "invalid parameters!"}`))
		return
	}

	err = RegisterArtist(&artist)

	// validação na interação com banco de dados
	if err != nil {
		// resposta HTTP de falha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))

}

// Busca todos os artistas do banco de dados
func SearchAllArtist(response http.ResponseWriter, request *http.Request) {

	artist := ArtistCollection()

	EncodeJson(response, artist)
}

// Excluir um artista do bd atravez do nome artistico dele
func DeleteArtist(response http.ResponseWriter, request *http.Request) {

	err := RemoveArtist(request.URL.Query().Get("nameartist"))

	if err != nil {
		response.Write([]byte("code: 400"))
		return
	}

	response.Write([]byte("sucess"))
	return
}

func ArtistName(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	artist := nameArtist{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &artist)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
	}

	err = validate.Struct(artist)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
	}

	err = FindArtistName(&artist)

	if err != nil {
		response.Write([]byte("500"))
		return
	}

	response.Write([]byte("200"))
}

func UpdateArtist(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	artist := Artist{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &artist)

	// Validando erro de decodificação
	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	// validação da informação
	err = validate.Struct(artist)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
		return
	}

	// regras de negócio para salvar usuario no banco de dados
	err = EditArtist(&artist)

	// Validação na interação com banco de dados
	if err != nil {
		// resposta HTTP de falaha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))

}
