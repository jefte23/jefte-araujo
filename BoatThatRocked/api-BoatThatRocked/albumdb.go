package main

import (
	"log"

	"github.com/globalsign/mgo/bson"
)

func RegisterAlbum(album *Album) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("album")

	err = collection.Insert(album)

	return
}

func AlbumCollection() (album []interface{}) {

	conn, _ := GetConnection()

	collection := conn.DB("BoatThatRocked").C("album")

	collection.Find(bson.M{}).All(&album)

	return
}

func RemoveAlbum(id string) (err error) {

	conn, err := GetConnection()

	collection := conn.DB("BoatThatRocked").C("album")

	collection.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	return
}

func FindAlbumName(album *AlbumName) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("album")

	err = collection.Find(bson.M{"name": album.Name}).All(&album)

	return
}

func EditAlbum(album *Album) (err error) {

	conn, err := GetConnection()

	if err != nil {
		log.Printf("[ERROR] could not get BD connection")
	}

	collection := conn.DB("BoatThatRocked").C("album")

	err = collection.Update(bson.M{"name": album.Name}, &album)

	return
}
