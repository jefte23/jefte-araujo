package main

import (
	"net/http"
)

// struct do json Usuario e a validação da informação passava via URL
type User struct {
	_id         string `json:"_id"`
	Name        string `json:"name" validate:"required"`
	UserName    string `json:"username" validate:"required"`
	Email       string `json:"email" validate:"required"`
	Password    string `json:"password" validate:"required"`
	DateOfBirth string `json:"dateOfBirth" validate:"required"`
}

// struct Json para Update do usuario
type UserUpdate struct {
	_id         int64  `json:"_id"`
	Name        string `json:"name"`
	UserName    string `json:"username"`
	Email       string `json:"email" validate:"required"`
	Password    string `json:"password" validate:"required"`
	DateOfBirth string `json:"dateOfBirth"`
}

// struct Json para Login do usuario
type UserLogin struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

// struct Json para Busca de usuarios pelo nome
type UserName struct {
	Name string `json:"name" validate:"required"`
}

// Função para registro/Cadastro de usuário
func InsertUser(response http.ResponseWriter, request *http.Request) {

	// retirando corpo da requisição
	body := request.Body

	// criando estrutura de usuário vazia
	user := User{}

	// decodificando o JSON no estrutura de usuário
	err := DecodeJson(body, &user)

	// validando erro de decodificação
	if err != nil {
		// resposta HTTP de formato inválido da informação
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	// validação da informação
	err = validate.Struct(user)

	// validando erro de validação
	if err != nil {
		// resposta HTTP de invalidação dos campos da requisição
		response.Write([]byte(`{"code": 400, "message": "invalid parameters!"}`))
		return
	}

	err = RegisterUser(&user)

	// validação na interação com banco de dados
	if err != nil {
		// resposta HTTP de falha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))

}

// Função para busca de um usuário no banco de dados
func SearchAllUsers(response http.ResponseWriter, request *http.Request) {

	users := UserCollection()

	EncodeJson(response, users)

}

// Função para excuir um usuario atravez da sua id
func DeleteUser(response http.ResponseWriter, request *http.Request) {

	err := RemoveUser(request.URL.Query().Get("id"))

	if err != nil {
		response.Write([]byte("code: 400"))
		return
	}

	response.Write([]byte("sucess"))
	return

}

func UpdateUser(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	user := UserUpdate{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &user)

	// Validando erro de decodificação
	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
		return
	}

	// validação da informação
	err = validate.Struct(user)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
		return
	}

	// regras de negócio para salvar usuario no banco de dados
	err = EditUser(&user)

	// Validação na interação com banco de dados
	if err != nil {
		// resposta HTTP de falaha interna do servidor
		response.Write([]byte("500"))
		return
	}

	// resposta de sucesso HTTP
	response.Write([]byte("200"))
}

func LoginUser(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	user := UserLogin{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &user)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
	}

	err = validate.Struct(user)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
	}

	err = AccessUser(&user)

	if err != nil {
		response.Write([]byte("500"))
		return
	}

	response.Write([]byte("200"))

}

func UsersName(response http.ResponseWriter, request *http.Request) {

	// retira corpo da requisição
	body := request.Body

	// Cria estrutura de usuário vazia
	user := UserName{}

	// decodifica o JSON na estrutura de usuário
	err := DecodeJson(body, &user)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "you know nothing!"}`))
	}

	err = validate.Struct(user)

	if err != nil {
		response.Write([]byte(`{"code": 400, "message": "Invalid parameters!"}`))
	}

	err = FindAllName(&user)

	if err != nil {
		response.Write([]byte("500"))
		return
	}

	response.Write([]byte("200"))
}
